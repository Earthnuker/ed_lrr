use ed_lrr::preprocess::{preprocess_files, PreprocessOpts};
use ed_lrr::route::{route, RouteOpts};
use humantime::format_duration;
use std::time::Instant;
use structopt::StructOpt;
#[derive(Debug, StructOpt)]
#[structopt(
    name = "ed_lrr",
    about = "Elite: Dangerous Long-Range Router",
    rename_all = "snake_case"
)]
enum Opts {
    /// Plots a route through multiple systems
    Route(RouteOpts),
    /// Preprocess EDSM Dump
    Preprocess(PreprocessOpts),
}

fn main() -> std::io::Result<()> {
    let t_start = Instant::now();
    let opts = Opts::from_args();
    let ret = match opts {
        Opts::Route(opts) => route(opts),
        Opts::Preprocess(opts) => preprocess_files(opts),
    };
    println!("Total time: {}", format_duration(t_start.elapsed()));
    ret
}
