use serde::{Deserialize, Serialize};
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SystemSerde {
    pub id: u32,
    pub star_type: String,
    pub system: String,
    pub body: String,
    pub mult: f32,
    pub distance: u32,
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl SystemSerde {
    pub fn build(&self) -> System {
        System {
            id: self.id,
            star_type: self.star_type.clone(),
            system: self.system.clone(),
            body: self.body.clone(),
            mult: self.mult,
            distance: self.distance,
            pos: [self.x, self.y, self.z],
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct System {
    pub id: u32,
    pub star_type: String,
    pub system: String,
    pub body: String,
    pub mult: f32,
    pub distance: u32,
    pub pos: [f32; 3],
}
